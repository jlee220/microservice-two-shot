# Wardrobify

Team:

* Chad Szymarek - Hats
* Jason Lee - Shoes

## Design

wardrobe will have the children of shoes and hats microservices
shoes and hats will have a one to many relationship with the wardrobe


## Shoes microservice

There will be a Shoe model in django with the manufacturer, model name, color, image, what bin it is located in. The data will be coming from the RESTful API and plugged into the react app.

## Hats microservice

Going to have a hats model that has fields for-
    fabric,
    style_name,
    color,
    picture_url,
    location where it exists

I'm going to grab the data that I need from the RESTapi so that I can use it in my hats microservices for the location and the bin. 
