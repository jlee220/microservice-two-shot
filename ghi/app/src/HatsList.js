import React, { useState } from "react";

function HatsList(props) {
  const [currentHat, setCurrentHat] = useState(props.hats[0]);

  const [currentHatLocation, setCurrentHatLocation] = useState();


  const getHatDetail = async () => {
    const url = `http://localhost:8090/api/hats/${currentHat.id}/`;
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCurrentHatLocation(data.location.closet_name);
    }
  }

  const deleteHat = async () => {
    fetch(`http://localhost:8090/api/hats/${currentHat.id}/`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    window.location.reload();
  }

  const handleClick = async (hat) => {
    console.log("setting current hat", hat);
    setCurrentHat(hat);
    getHatDetail();
  };

  return (
    <>
      <table className="table table-striped table-hover">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style name</th>
            <th>Color</th>
            <th>Remove</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map((hat) => {
            return (
              <tr
                key={hat.id}
                onClick={() => handleClick(hat)}
                data-bs-toggle="modal"
                data-bs-target="#hatModal"
              >
                <td>{hat.fabric}</td>
                <td>{hat.style_name}</td>
                <td>{hat.color}</td>
                <td>
                  <button onClick={() => deleteHat()} type="button">
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div
        className="modal fade"
        id="hatModal"
        tabIndex="-1"
        aria-labelledby="hatModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            {currentHat && (
              <>
                <div className="modal-body">
                  <img
                    src={currentHat.picture_url}
                    className="rounded mx-auto d-block img-fluid"
                  />
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th>Fabric</th>
                        <th>Style name</th>
                        <th>Color</th>
                        <th>Location</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{currentHat.fabric}</td>
                        <td>{currentHat.style_name}</td>
                        <td>{currentHat.color}</td>
                        <td>{currentHatLocation}</td>
                      </tr>
                    </tbody>
                  </table>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-secondary"
                      data-bs-dismiss="modal"
                    >
                      Close
                    </button>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </>
  );
}

export default HatsList;
